# Anywhen

Parsing date and time expressed in natural language and returning a datetime object.

## Terms

### Relational descriptors

#### Unary

- Before today
    + ago
    + prior

#### Binary

- Subtract from the following
    + before
    + preceding
    + ante

- Add to the following
    + after
    + following
    + post

### Spans

+ second
+ minute
+ hour
+ day
+ week
+ month
+ year