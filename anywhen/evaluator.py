from typing import Any

from .term_resolver import TermResolver


class Evaluator:
    @classmethod
    def collapse_numerics(cls, tokens: list[Any]):
        """Find every sequence of numeric words
        and collapse them down to the values they represent.

        Example:
            ['three', 'hundred', 'fifty', 'nuts', 'and', 'two', 'thousand', 'bolts']
                would be collapsed into
            [350, 'nuts', 'and', 2000, 'bolts']
        """

        # Find and replace all incremental numeric sequences
        # with their represented value
        while True:
            slice_ = cls._find_number_sequence(tokens)

            # Stop once no sequence remains
            if slice_ is None:
                break

            # Replace slice of sequence with the represented value
            tokens[slice_] = [cls._join_numbers(tokens[slice_])]

        return tokens

    @classmethod
    def _find_number_sequence(cls, tokens: list[Any]):
        """Find a sequence of numbers and return its slice."""
        
        start, end = None, None

        for i, token in enumerate(tokens):
            if isinstance(token, (int, float)):
                if start is None:
                    start = i
                end = i + 1

            else:
                # Stop searching if a slice of 2 or more elements has been located
                if (end or 0) - (start or 0) >= 2:
                    return slice(start, end)

                # Otherwise, start over
                start, end = None, None

        # Only return slice if it covers 2 or more elements
        if (end or 0) - (start or 0) < 2:
            return None
        return slice(start, end)

    @classmethod
    def _join_numbers(cls, numbers: list[int | float]):
        """Recursively join a sequence of numbers into the represented value."""

        if len(numbers) == 0:
            return None

        if len(numbers) == 1:
            return numbers[0]

        # Multiply the right-hand number by the joint value of all numbers
        # reaching down to the first number that supercedes the right-hand number.
        # Add that to the joint value of everything to the left of and including the supercedant.
        
        supercedant = cls._index_supercedant(numbers[:-1], numbers[-1])
        a = cls._join_numbers(numbers[:supercedant + 1])
        b = cls._join_numbers(numbers[supercedant + 1:-1])

        # `a` and `b` should have no effect if None
        if a is None:
            a = 0
        if b is None:
            b = 1
        
        return a + b * numbers[-1]

    @classmethod
    def _index_supercedant(cls, numbers: list[int | float], value: int | float):
        """Get index of the first number from the right that supercedes or equals a value."""
        
        for i in range(len(numbers) - 1, -1, -1):
            if numbers[i] >= value:
                return i
        return -1
