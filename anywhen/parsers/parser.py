import operator

from collections import OrderedDict
from functools import reduce

from ..term_resolver import TermResolver
from ..evaluator import Evaluator


class Parser:
    def __init__(self):
        self.term_resolver = TermResolver(OrderedDict(self.get_dictionary()))

    def parse(self, text: str):
        """Parse text into a single, represented value."""

        # Tokenise text into terms
        tokens = [word.rstrip(',').lower() for word in text.split()]
        tokens = self.term_resolver.resolve(tokens)

        # Convert to numerics
        tokens = Evaluator.collapse_numerics(tokens)

        # Collapse into one value
        values = [0]
        while tokens:
            token = tokens.pop()
            
            if isinstance(token, (int, float)):
                values[-1] *= token
            else:
                values.append(token)

        return reduce(operator.add, values[1:])

    def get_dictionary(self):
        raise NotImplementedError()