from typing import Any
from collections import OrderedDict


class TermResolver:
    def __init__(self, nouns: dict[str, object]):
        # Split words by spaces into a tuple and lowercase them
        nouns = {
            tuple(words.lower().split(" ")): obj
            for words, obj in nouns.items()
        }

        # Sort nouns by word count, most to least
        self.nouns = OrderedDict([
            (words, nouns[words])
            for words in sorted(nouns.keys(), key=len)[::-1]
        ])

    def resolve(self, tokens: list[Any]):
        """Resolve nouns into objects."""
        
        resolved_tokens = []

        # Lowercase tokens for matching
        tokens = list(map(str.lower, tokens))

        i = 0
        while i < len(tokens):
            for words, obj in self.nouns.items():
                if tuple(tokens[i:i + len(words)]) == words:
                    resolved_tokens.append(obj)
                    i += len(words)
                    break
            else:
                resolved_tokens.append(tokens[i])
                i += 1

        return resolved_tokens